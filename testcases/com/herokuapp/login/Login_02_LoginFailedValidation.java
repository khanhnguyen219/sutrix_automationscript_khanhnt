package com.herokuapp.login;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Login_02_LoginFailedValidation {
	WebDriver driver;
	WebDriverWait wait;
	String userName;
	String passWord;

	@BeforeClass
	public void beforeClass() {
		userName = "tomsmith";
		passWord = "SuperSecretPassword!";
		
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
//		System.setProperty("webdriver.gecko.driver", ".\\driver\\geckodriver.exe");

		driver = new ChromeDriver();
//		driver = new FirefoxDriver();
		
		wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void TestScript_01() {
		// Test Script - 01: Verify displayed 'Your username is invalid!'
		// message after click on Login Button without input data into both
		// Username & Password textbox.

		// Step 01: Visit to 'https://the-internet.herokuapp.com/login' url.
		driver.get("https://the-internet.herokuapp.com/login");
		String herokuappLoginUrl = driver.getCurrentUrl();
		Assert.assertEquals("https://the-internet.herokuapp.com/login", herokuappLoginUrl);

		// Step 02: Click on login login button.
		WebElement loginButton = driver.findElement(By.xpath("//button[@class = 'radius']//i[text() = ' Login']"));
		loginButton.click();

		// Step 03: Verify displayed 'Your username is invalid!' message.
		WebElement errorMessage = driver.findElement(By.xpath("//div[contains(text(), 'Your username is invalid!')]"));
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		Assert.assertTrue(errorMessage.isDisplayed());

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
