package com.herokuapp.login;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Login_01_LoginSuccessfulValidation {
	WebDriver driver;
	WebDriverWait wait;
	String userName;
	String passWord;

	@BeforeClass
	public void beforeClass() {
		userName = "tomsmith";
		passWord = "SuperSecretPassword!";

		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		// System.setProperty("webdriver.gecko.driver",
		// ".\\driver\\geckodriver.exe");

		driver = new ChromeDriver();
		// driver = new FirefoxDriver();

		wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void TestScript_01() {
		// Test Script - 01: Verify login successful by submit login form after
		// entered
		// valid Username
		// and Password !

		// Step 01: Visit to 'https://the-internet.herokuapp.com/login' url.
		driver.get("https://the-internet.herokuapp.com/login");
		String herokuappLoginUrl = driver.getCurrentUrl();
		Assert.assertEquals("https://the-internet.herokuapp.com/login", herokuappLoginUrl);

		// Step 02: Input Username & Password.
		WebElement usernameTextbox = driver.findElement(By.xpath("//input[@id='username']"));
		usernameTextbox.sendKeys(userName);
		WebElement passwordTextbox = driver.findElement(By.xpath("//input[@id='password']"));
		passwordTextbox.sendKeys(passWord);

		// Step 03: Submit login form.

		// usernameTextbox.submit();
		passwordTextbox.submit();

		// Step 04: Verify displayed 'Welcome to the Secure Area. When you are
		// done click logout below.' text after login successful
		wait.until(ExpectedConditions.urlMatches("https://the-internet.herokuapp.com/secure"));
		WebElement loginsuccessMessage = driver.findElement(
				By.xpath("//h4[contains(.,'Welcome to the Secure Area. When you are done click logout below.')]"));
		Assert.assertTrue(loginsuccessMessage.isDisplayed());
	}

	@Test
	public void TestScript_02() {
		// Test Script - 02: Verify login successful by clicked on login button
		// after
		// entered valid
		// Username
		// and Password !

		// Step 01: Visit to 'https://the-internet.herokuapp.com/login' url.
		driver.get("https://the-internet.herokuapp.com/login");
		String herokuappLoginUrl = driver.getCurrentUrl();
		Assert.assertEquals("https://the-internet.herokuapp.com/login", herokuappLoginUrl);

		// Step 02: Input Username & Password.
		WebElement usernameTextbox = driver.findElement(By.xpath("//input[@id='username']"));
		usernameTextbox.sendKeys(userName);
		WebElement passwordTextbox = driver.findElement(By.xpath("//input[@id='password']"));
		passwordTextbox.sendKeys(passWord);

		// Step 03: Click on login login button.
		WebElement loginButton = driver.findElement(By.xpath("//button[@class = 'radius']//i[text() = ' Login']"));
		loginButton.click();

		// Step 04: Verify displayed 'Welcome to the Secure Area. When you are
		// done click logout below.' text after login successful
		wait.until(ExpectedConditions.urlMatches("https://the-internet.herokuapp.com/secure"));
		WebElement loginsuccessMessage = driver.findElement(
				By.xpath("//h4[contains(.,'Welcome to the Secure Area. When you are done click logout below.')]"));
		Assert.assertTrue(loginsuccessMessage.isDisplayed());

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
